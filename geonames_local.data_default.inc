<?php
/**
 * @file
 * geonames_local.data_default.inc
 */

/**
 * Implements hook_data_default().
 */
function geonames_local_data_default() {
  $export = array();

  $data_table = new stdClass();
  $data_table->disabled = FALSE; /* Edit this to true to make a default data_table disabled initially */
  $data_table->api_version = 1;
  $data_table->title = 'Geonames CountryInfo';
  $data_table->name = 'geonames_countryinfo';
  $data_table->table_schema = array(
    'description' => '',
    'fields' => array(
      'iso_alpha2' => array(
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 2,
        'description' => 'two-letter country code (ISO 3166-1-alpha-2)',
      ),
      'iso_alpha3' => array(
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 3,
        'description' => 'three-letter country code (ISO 3166-1 alpha-3)',
      ),
      'iso_numeric' => array(
        'type' => 'int',
        'not null' => TRUE,
        'size' => 'small',
        'unsigned' => TRUE,
        'description' => 'three-digit country code (ISO 3166-1 numeric)',
      ),
      'fips_code' => array(
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 3,
        'description' => 'Federal Information Processing Standards (FIPS) country code',
      ),
      'name' => array(
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 60,
        'description' => 'Country name',
      ),
      'capital' => array(
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 60,
        'description' => 'Capital',
      ),
      'areainsqkm' => array(
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 11,
        'description' => 'Area(in sq km)',
      ),
      'population' => array(
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 11,
        'description' => 'Population',
      ),
      'continent' => array(
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 2,
        'description' => 'Continent, one of [AF, AS, EU, NA, OC, SA, AN]',
      ),
      'tld' => array(
        'type' => 'varchar',
        'length' => '200',
        'not null' => FALSE,
        'description' => 'Internet Top-Level Domain (including leading dot)',
      ),
      'currency' => array(
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 3,
        'description' => 'Currency Code',
      ),
      'currencyname' => array(
        'type' => 'varchar',
        'length' => '200',
        'not null' => FALSE,
        'description' => 'Currency Name',
      ),
      'phone' => array(
        'type' => 'varchar',
        'length' => '200',
        'not null' => FALSE,
        'description' => 'IDD Phone Prefix',
      ),
      'postal_code_format' => array(
        'type' => 'varchar',
        'length' => '200',
        'not null' => FALSE,
        'description' => 'Postal Code Format',
      ),
      'postal_code_regex' => array(
        'type' => 'varchar',
        'length' => '200',
        'not null' => FALSE,
        'description' => 'Postal Code Regex',
      ),
      'languages' => array(
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 100,
        'description' => 'Lists of languages spoken in a country ordered by the number of speakers',
      ),
      'geonameid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'Unique geonames identifier',
      ),
      'neighbours' => array(
        'type' => 'varchar',
        'length' => '200',
        'not null' => FALSE,
        'description' => 'two-letter codes for countries that border this country',
      ),
      'equivalentfipscode' => array(
        'type' => 'varchar',
        'length' => '200',
        'not null' => FALSE,
        'description' => 'Equivalent Fips Code',
      ),
    ),
    'name' => 'geonames_city_information',
    'indexes' => array(
      'iso_alpha3' => array(
        0 => 'iso_alpha3',
      ),
      'iso_numeric' => array(
        0 => 'iso_numeric',
      ),
      'fips_code' => array(
        0 => 'fips_code',
      ),
      'continent' => array(
        0 => 'continent',
      ),
      'currency' => array(
        0 => 'currency',
      ),
      'iso_alpha2' => array(
        0 => 'iso_alpha2',
      ),
    ),
    'primary key' => array(
      0 => 'geonameid',
    ),
  );
  $data_table->meta = array(
    'fields' => array(
      'geonameid' => array(
        'label' => 'geonameid',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'iso_alpha2' => array(
        'label' => 'ISO 3166-1-alpha-2',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'iso_alpha3' => array(
        'label' => 'ISO 3166-1-alpha-3',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'iso_numeric' => array(
        'label' => 'ISO Numeric',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'fips_code' => array(
        'label' => 'FIPS country code.',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'name' => array(
        'label' => 'Country name',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'capital' => array(
        'label' => 'Capital',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'areainsqkm' => array(
        'label' => 'Area (in sq km)',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'population' => array(
        'label' => 'Population',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'continent' => array(
        'label' => 'Continent [AF, AS, EU, NA, OC, SA, AN]',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'tld' => array(
        'label' => 'Internet Top-Level Domain (including leading dot)',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'currency' => array(
        'label' => 'Currency Code',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'currencyname' => array(
        'label' => 'Currency Name',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'phone' => array(
        'label' => 'Phone',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'postal_code_format' => array(
        'label' => 'Postal Code Format',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'postal_code_regex' => array(
        'label' => 'Postal Code Regex',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'languages' => array(
        'label' => 'Lists of languages spoken in a country ordered by the number of speakers',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'neighbours' => array(
        'label' => 'two-letter codes for countries that border this country',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'equivalentfipscode' => array(
        'label' => 'Equivalent fips Code',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
    ),
    'label_field' => '',
    'is_entity_type' => 1,
    'entity_id' => 'geonameid',
    'entity_name' => 'iso_alpha2',
  );
  $export['geonames_countryinfo'] = $data_table;

  $data_table = new stdClass();
  $data_table->disabled = FALSE; /* Edit this to true to make a default data_table disabled initially */
  $data_table->api_version = 1;
  $data_table->title = 'Geonames Features';
  $data_table->name = 'geonames_feature';
  $data_table->table_schema = array(
    'description' => '',
    'fields' => array(
      'geonameid' => array(
        'type' => 'int',
        'size' => 'big',
        'not null' => TRUE,
        'description' => '',
      ),
      'name' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'asciiname' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'alternatenames' => array(
        'type' => 'text',
        'size' => 'normal',
        'not null' => FALSE,
        'description' => '',
      ),
      'latitude' => array(
        'type' => 'float',
        'size' => 'big',
        'not null' => FALSE,
        'description' => '',
      ),
      'longitude' => array(
        'type' => 'float',
        'size' => 'big',
        'not null' => FALSE,
        'description' => '',
      ),
      'feature_class' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'feature_code' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'country_code' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'country_geonameid' => array(
        'type' => 'int',
        'size' => 'big',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'description' => '',
      ),
      'cc2' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'admin1_code' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'admin2_code' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'admin3_code' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'admin4_code' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'population' => array(
        'type' => 'int',
        'size' => 'big',
        'not null' => FALSE,
        'description' => '',
      ),
      'elevation' => array(
        'type' => 'int',
        'size' => 'big',
        'not null' => FALSE,
        'description' => '',
      ),
      'dem' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'timezone' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'modification_date' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'lat_lon_pair' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
    ),
    'name' => 'geonames_feature',
    'indexes' => array(
      'country_code' => array(
        0 => 'country_code',
      ),
      'country_geonameid' => array(
        0 => 'country_geonameid',
      ),
      'feature_class' => array(
        0 => 'feature_class',
      ),
      'latitude' => array(
        0 => 'latitude',
      ),
      'longitude' => array(
        0 => 'longitude',
      ),
      'name' => array(
        0 => 'name',
      ),
      'lat_lon_pair' => array(
        0 => 'lat_lon_pair',
      ),
    ),
    'primary key' => array(
      0 => 'geonameid',
    ),
  );
  $data_table->meta = array(
    'fields' => array(
      'geonameid' => array(
        'label' => 'geonameid',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'name' => array(
        'label' => 'name',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'asciiname' => array(
        'label' => 'asciiname',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'alternatenames' => array(
        'label' => 'alternatenames',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'latitude' => array(
        'label' => 'latitude',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'longitude' => array(
        'label' => 'longitude',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'feature_class' => array(
        'label' => 'feature class',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'feature_code' => array(
        'label' => 'feature code',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'country_code' => array(
        'label' => 'country code',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'country_geonameid' => array(
        'label' => 'country geonameid',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'cc2' => array(
        'label' => 'cc2',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'admin1_code' => array(
        'label' => 'admin1 code',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'admin2_code' => array(
        'label' => 'admin2 code',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'admin3_code' => array(
        'label' => 'admin3 code',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'admin4_code' => array(
        'label' => 'admin4 code',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'population' => array(
        'label' => 'population',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'elevation' => array(
        'label' => 'elevation',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'dem' => array(
        'label' => 'dem',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'timezone' => array(
        'label' => 'timezone',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'changed' => array(
        'label' => 'modification date',
        'locked' => FALSE,
        'required' => FALSE,
      ),
      'lat_lon_pair' => array(
        'label' => 'lat lon pair',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'modification_date' => array(
        'label' => 'modification date',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => 'date',
          'granularity' => 'day',
        ),
      ),
    ),
    'label_field' => '',
    'is_entity_type' => 1,
    'entity_id' => 'geonameid',
    'join' => array(
      'geonames_countryinfo' => array(
        'left_field' => 'geonameid',
        'field' => 'country_geonameid',
        'inner_join' => '0',
      ),
      'geonames_postalcode' => array(
        'left_field' => 'place_name',
        'field' => 'name',
        'inner_join' => '0',
      ),
    ),
  );
  $export['geonames_feature'] = $data_table;

  $data_table = new stdClass();
  $data_table->disabled = FALSE; /* Edit this to true to make a default data_table disabled initially */
  $data_table->api_version = 1;
  $data_table->title = 'Geonames PostalCode';
  $data_table->name = 'geonames_postalcode';
  $data_table->table_schema = array(
    'description' => '',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'size' => 'big',
        'not null' => TRUE,
        'description' => '',
        'unsigned' => FALSE,
      ),
      'country_code' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'country_geonameid' => array(
        'type' => 'int',
        'size' => 'big',
        'not null' => TRUE,
        'description' => '',
        'unsigned' => TRUE,
      ),
      'postal_code' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => TRUE,
        'description' => '',
      ),
      'place_name' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'admin_name1' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'admin_code1' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'admin_name2' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'admin_code2' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'admin_name3' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'admin_code3' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => '',
      ),
      'latitude' => array(
        'type' => 'float',
        'size' => 'big',
        'not null' => FALSE,
        'description' => '',
      ),
      'longitude' => array(
        'type' => 'float',
        'size' => 'big',
        'not null' => FALSE,
        'description' => '',
      ),
      'accuracy' => array(
        'type' => 'int',
        'size' => 'small',
        'not null' => FALSE,
        'description' => '',
      ),
      'lat_lon_pair' => array(
        'type' => 'varchar',
        'length' => 255,
      ),
    ),
    'name' => 'geonames_postalcode',
    'indexes' => array(
      'country_code' => array(
        0 => 'country_code',
      ),
      'country_geonameid' => array(
        0 => 'country_geonameid',
      ),
      'postal_code' => array(
        0 => 'postal_code',
      ),
      'place_name' => array(
        0 => 'place_name',
      ),
      'admin_code1' => array(
        0 => 'admin_code1',
      ),
      'admin_code2' => array(
        0 => 'admin_code2',
      ),
      'admin_code3' => array(
        0 => 'admin_code3',
      ),
      'latitude' => array(
        0 => 'latitude',
      ),
      'longitude' => array(
        0 => 'longitude',
      ),
      'lat_lon_pair' => array(
        0 => 'lat_lon_pair',
      ),
    ),
    'primary key' => array(
      0 => 'id',
    ),
  );
  $data_table->meta = array(
    'fields' => array(
      'id' => array(
        'label' => 'id',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'country_code' => array(
        'label' => 'Country code ISO2',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'country_geonameid' => array(
        'label' => 'Country geonameid',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'postal_code' => array(
        'label' => 'Postal code',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'place_name' => array(
        'label' => 'Place name',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'admin_name1' => array(
        'label' => 'Admin name 1',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'admin_code1' => array(
        'label' => 'Admin code 1',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'admin_name2' => array(
        'label' => 'Admin name 2',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'admin_code2' => array(
        'label' => 'Admin code 2',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'admin_name3' => array(
        'label' => 'Admin name 3',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'admin_code3' => array(
        'label' => 'Admin code 3',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'latitude' => array(
        'label' => 'latitude',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'longitude' => array(
        'label' => 'longitude',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
      'accuracy' => array(
        'label' => 'Accuracy',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => 'date',
          'granularity' => 'day',
        ),
      ),
      'lat_lon_pair' => array(
        'label' => 'Lat/Lon pair',
        'locked' => FALSE,
        'required' => FALSE,
        'date' => array(
          'sql_type' => '',
          'granularity' => '',
        ),
      ),
    ),
    'label_field' => '',
    'is_entity_type' => 1,
    'entity_id' => 'id',
    'join' => array(
      'geonames_countryinfo' => array(
        'left_field' => 'geonameid',
        'field' => 'country_geonameid',
        'inner_join' => '0',
      ),
      'geonames_feature' => array(
        'left_field' => 'name',
        'field' => 'place_name',
        'inner_join' => '0',
      ),
    ),
    'entity_name' => '',
  );
  $export['geonames_postalcode'] = $data_table;

  return $export;
}
