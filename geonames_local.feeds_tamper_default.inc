<?php
/**
 * @file
 * geonames_local.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function geonames_local_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'geonames_feature-blank_source_1-copy';
  $feeds_tamper->importer = 'geonames_feature';
  $feeds_tamper->source = 'blank source 1';
  $feeds_tamper->plugin_id = 'copy';
  $feeds_tamper->settings = array(
    'to_from' => 'from',
    'source' => '8',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Copy source value';
  $export['geonames_feature-blank_source_1-copy'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'geonames_feature-blank_source_1-efq_finder';
  $feeds_tamper->importer = 'geonames_feature';
  $feeds_tamper->source = 'blank source 1';
  $feeds_tamper->plugin_id = 'efq_finder';
  $feeds_tamper->settings = array(
    'update' => 'Update',
    'entity_type' => 'data_geonames_countryinfo',
    'bundle' => '',
    'field' => 'iso_alpha2',
    'property' => TRUE,
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Entity Field Query finder';
  $export['geonames_feature-blank_source_1-efq_finder'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'geonames_feature-lat_lon_pair-rewrite';
  $feeds_tamper->importer = 'geonames_feature';
  $feeds_tamper->source = '4';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[4],[5]',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Rewrite';
  $export['geonames_feature-lat_lon_pair-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'geonames_postalcode-blank_source_1-copy';
  $feeds_tamper->importer = 'geonames_postalcode';
  $feeds_tamper->source = 'blank source 1';
  $feeds_tamper->plugin_id = 'copy';
  $feeds_tamper->settings = array(
    'to_from' => 'from',
    'source' => '0',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Copy source value';
  $export['geonames_postalcode-blank_source_1-copy'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'geonames_postalcode-blank_source_1-efq_finder';
  $feeds_tamper->importer = 'geonames_postalcode';
  $feeds_tamper->source = 'blank source 1';
  $feeds_tamper->plugin_id = 'efq_finder';
  $feeds_tamper->settings = array(
    'update' => 'Update',
    'entity_type' => 'data_geonames_countryinfo',
    'bundle' => '',
    'field' => 'iso_alpha2',
    'property' => TRUE,
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Entity Field Query finder';
  $export['geonames_postalcode-blank_source_1-efq_finder'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'geonames_postalcode-blank_source_2-rewrite';
  $feeds_tamper->importer = 'geonames_postalcode';
  $feeds_tamper->source = 'blank source 2';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[9],[10]',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Rewrite';
  $export['geonames_postalcode-blank_source_2-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'geonames_postalcode-blank_source_3-rewrite';
  $feeds_tamper->importer = 'geonames_postalcode';
  $feeds_tamper->source = 'blank source 3';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[0]|[1]|[2]',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Rewrite';
  $export['geonames_postalcode-blank_source_3-rewrite'] = $feeds_tamper;

  return $export;
}
