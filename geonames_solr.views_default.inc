<?php
/**
 * @file
 * geonames_solr.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function geonames_solr_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'geonames_feature';
  $view->description = 'Provide Genoames Feature Information';
  $view->tag = 'Geonames';
  $view->base_table = 'search_api_index_geonames_feature';
  $view->human_name = 'Geonames Features View';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Geonames Coordinate Feature';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Indexed Geonames Features: geonameid */
  $handler->display->display_options['fields']['geonameid']['id'] = 'geonameid';
  $handler->display->display_options['fields']['geonameid']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['geonameid']['field'] = 'geonameid';
  /* Field: Indexed Geonames Features: name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['link_to_entity'] = 0;
  /* Field: Indexed Geonames Features: asciiname */
  $handler->display->display_options['fields']['asciiname']['id'] = 'asciiname';
  $handler->display->display_options['fields']['asciiname']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['asciiname']['field'] = 'asciiname';
  $handler->display->display_options['fields']['asciiname']['link_to_entity'] = 0;
  /* Field: Indexed Geonames Features: alternatenames */
  $handler->display->display_options['fields']['alternatenames']['id'] = 'alternatenames';
  $handler->display->display_options['fields']['alternatenames']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['alternatenames']['field'] = 'alternatenames';
  $handler->display->display_options['fields']['alternatenames']['link_to_entity'] = 0;
  /* Field: Indexed Geonames Features: country code */
  $handler->display->display_options['fields']['country_code']['id'] = 'country_code';
  $handler->display->display_options['fields']['country_code']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['country_code']['field'] = 'country_code';
  $handler->display->display_options['fields']['country_code']['label'] = 'country_code';
  $handler->display->display_options['fields']['country_code']['link_to_entity'] = 0;
  /* Field: Related postal code: Postal code (indexed) */
  $handler->display->display_options['fields']['postalcode_postal_code']['id'] = 'postalcode_postal_code';
  $handler->display->display_options['fields']['postalcode_postal_code']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['postalcode_postal_code']['field'] = 'postalcode_postal_code';
  $handler->display->display_options['fields']['postalcode_postal_code']['label'] = 'postalcode';
  $handler->display->display_options['fields']['postalcode_postal_code']['link_to_entity'] = 0;
  /* Field: Indexed Geonames Features: admin1 code */
  $handler->display->display_options['fields']['admin1_code']['id'] = 'admin1_code';
  $handler->display->display_options['fields']['admin1_code']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['admin1_code']['field'] = 'admin1_code';
  $handler->display->display_options['fields']['admin1_code']['label'] = 'admin1_code';
  $handler->display->display_options['fields']['admin1_code']['link_to_entity'] = 0;
  /* Field: Indexed Geonames Features: admin2 code */
  $handler->display->display_options['fields']['admin2_code']['id'] = 'admin2_code';
  $handler->display->display_options['fields']['admin2_code']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['admin2_code']['field'] = 'admin2_code';
  $handler->display->display_options['fields']['admin2_code']['label'] = 'admin2_code';
  $handler->display->display_options['fields']['admin2_code']['link_to_entity'] = 0;
  /* Field: Indexed Geonames Features: admin3 code */
  $handler->display->display_options['fields']['admin3_code']['id'] = 'admin3_code';
  $handler->display->display_options['fields']['admin3_code']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['admin3_code']['field'] = 'admin3_code';
  $handler->display->display_options['fields']['admin3_code']['label'] = 'admin3_code';
  $handler->display->display_options['fields']['admin3_code']['link_to_entity'] = 0;
  /* Field: Indexed Geonames Features: admin4 code */
  $handler->display->display_options['fields']['admin4_code']['id'] = 'admin4_code';
  $handler->display->display_options['fields']['admin4_code']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['admin4_code']['field'] = 'admin4_code';
  $handler->display->display_options['fields']['admin4_code']['label'] = 'admin4_code';
  $handler->display->display_options['fields']['admin4_code']['link_to_entity'] = 0;
  /* Field: Indexed Geonames Features: cc2 */
  $handler->display->display_options['fields']['cc2']['id'] = 'cc2';
  $handler->display->display_options['fields']['cc2']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['cc2']['field'] = 'cc2';
  $handler->display->display_options['fields']['cc2']['link_to_entity'] = 0;
  /* Field: Indexed Geonames Features: dem */
  $handler->display->display_options['fields']['dem']['id'] = 'dem';
  $handler->display->display_options['fields']['dem']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['dem']['field'] = 'dem';
  $handler->display->display_options['fields']['dem']['link_to_entity'] = 0;
  /* Field: Indexed Geonames Features: elevation */
  $handler->display->display_options['fields']['elevation']['id'] = 'elevation';
  $handler->display->display_options['fields']['elevation']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['elevation']['field'] = 'elevation';
  $handler->display->display_options['fields']['elevation']['link_to_entity'] = 0;
  /* Field: Indexed Geonames Features: feature class */
  $handler->display->display_options['fields']['feature_class']['id'] = 'feature_class';
  $handler->display->display_options['fields']['feature_class']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['feature_class']['field'] = 'feature_class';
  $handler->display->display_options['fields']['feature_class']['label'] = 'feature_class';
  $handler->display->display_options['fields']['feature_class']['link_to_entity'] = 0;
  /* Field: Indexed Geonames Features: feature code */
  $handler->display->display_options['fields']['feature_code']['id'] = 'feature_code';
  $handler->display->display_options['fields']['feature_code']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['feature_code']['field'] = 'feature_code';
  $handler->display->display_options['fields']['feature_code']['label'] = 'feature_code';
  $handler->display->display_options['fields']['feature_code']['link_to_entity'] = 0;
  /* Field: Indexed Geonames Features: latitude */
  $handler->display->display_options['fields']['latitude']['id'] = 'latitude';
  $handler->display->display_options['fields']['latitude']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['latitude']['field'] = 'latitude';
  $handler->display->display_options['fields']['latitude']['link_to_entity'] = 0;
  /* Field: Indexed Geonames Features: longitude */
  $handler->display->display_options['fields']['longitude']['id'] = 'longitude';
  $handler->display->display_options['fields']['longitude']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['longitude']['field'] = 'longitude';
  $handler->display->display_options['fields']['longitude']['link_to_entity'] = 0;
  /* Field: Indexed Geonames Features: modification date */
  $handler->display->display_options['fields']['modification_date']['id'] = 'modification_date';
  $handler->display->display_options['fields']['modification_date']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['modification_date']['field'] = 'modification_date';
  $handler->display->display_options['fields']['modification_date']['link_to_entity'] = 0;
  /* Field: Indexed Geonames Features: population */
  $handler->display->display_options['fields']['population']['id'] = 'population';
  $handler->display->display_options['fields']['population']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['population']['field'] = 'population';
  $handler->display->display_options['fields']['population']['link_to_entity'] = 0;
  /* Field: Indexed Geonames Features: timezone */
  $handler->display->display_options['fields']['timezone']['id'] = 'timezone';
  $handler->display->display_options['fields']['timezone']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['timezone']['field'] = 'timezone';
  $handler->display->display_options['fields']['timezone']['link_to_entity'] = 0;
  /* Field: Indexed Geonames Features: lat lon pair */
  $handler->display->display_options['fields']['lat_lon_pair']['id'] = 'lat_lon_pair';
  $handler->display->display_options['fields']['lat_lon_pair']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['lat_lon_pair']['field'] = 'lat_lon_pair';
  $handler->display->display_options['fields']['lat_lon_pair']['label'] = 'distance';
  $handler->display->display_options['fields']['lat_lon_pair']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['lat_lon_pair']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['lat_lon_pair']['show_distance_to_point'] = TRUE;
  $handler->display->display_options['fields']['lat_lon_pair']['conversion_factor'] = '1';
  $handler->display->display_options['fields']['lat_lon_pair']['precision'] = '0';
  /* Field: country geonameid: Continent [AF, AS, EU, NA, OC, SA, AN] (indexed) */
  $handler->display->display_options['fields']['country_geonameid_continent']['id'] = 'country_geonameid_continent';
  $handler->display->display_options['fields']['country_geonameid_continent']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['fields']['country_geonameid_continent']['field'] = 'country_geonameid_continent';
  $handler->display->display_options['fields']['country_geonameid_continent']['label'] = 'continent';
  $handler->display->display_options['fields']['country_geonameid_continent']['link_to_entity'] = 0;
  /* Sort criterion: Indexed Geonames Features: lat lon pair */
  $handler->display->display_options['sorts']['lat_lon_pair']['id'] = 'lat_lon_pair';
  $handler->display->display_options['sorts']['lat_lon_pair']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['sorts']['lat_lon_pair']['field'] = 'lat_lon_pair';
  /* Contextual filter: Indexed Geonames Features: lat lon pair */
  $handler->display->display_options['arguments']['lat_lon_pair']['id'] = 'lat_lon_pair';
  $handler->display->display_options['arguments']['lat_lon_pair']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['arguments']['lat_lon_pair']['field'] = 'lat_lon_pair';
  $handler->display->display_options['arguments']['lat_lon_pair']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['lat_lon_pair']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['lat_lon_pair']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['lat_lon_pair']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['lat_lon_pair']['not'] = 0;
  $handler->display->display_options['arguments']['lat_lon_pair']['radius'] = '40000';
  /* Contextual filter: Indexed Geonames Features: feature class */
  $handler->display->display_options['arguments']['feature_class']['id'] = 'feature_class';
  $handler->display->display_options['arguments']['feature_class']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['arguments']['feature_class']['field'] = 'feature_class';
  $handler->display->display_options['arguments']['feature_class']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['feature_class']['skip_argument_processing_on_failure'] = FALSE;
  $handler->display->display_options['arguments']['feature_class']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['feature_class']['break_phrase'] = 1;
  $handler->display->display_options['arguments']['feature_class']['not'] = 0;
  /* Contextual filter: Indexed Geonames Features: feature code */
  $handler->display->display_options['arguments']['feature_code']['id'] = 'feature_code';
  $handler->display->display_options['arguments']['feature_code']['table'] = 'search_api_index_geonames_feature';
  $handler->display->display_options['arguments']['feature_code']['field'] = 'feature_code';
  $handler->display->display_options['arguments']['feature_code']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['feature_code']['skip_argument_processing_on_failure'] = FALSE;
  $handler->display->display_options['arguments']['feature_code']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['feature_code']['break_phrase'] = 1;
  $handler->display->display_options['arguments']['feature_code']['not'] = 0;

  /* Display: Coordinates Feature */
  $handler = $view->new_display('feed', 'Coordinates Feature', 'feed_coordinates_feature');
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_geojson_feed';
  $handler->display->display_options['style_options']['data_source']['value'] = 'latlon';
  $handler->display->display_options['style_options']['data_source']['latitude'] = 'latitude';
  $handler->display->display_options['style_options']['data_source']['longitude'] = 'longitude';
  $handler->display->display_options['style_options']['data_source']['wkt'] = 'geonameid';
  $handler->display->display_options['style_options']['data_source']['name_field'] = 'name';
  $handler->display->display_options['style_options']['data_source']['description_field'] = 'name';
  $handler->display->display_options['style_options']['jsonp_prefix'] = '';
  $handler->display->display_options['path'] = 'geonames/coords';
  $translatables['geonames_feature'] = array(
    t('Master'),
    t('Geonames Coordinate Feature'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('geonameid'),
    t('.'),
    t(','),
    t('name'),
    t('asciiname'),
    t('alternatenames'),
    t('country_code'),
    t('postalcode'),
    t('admin1_code'),
    t('admin2_code'),
    t('admin3_code'),
    t('admin4_code'),
    t('cc2'),
    t('dem'),
    t('elevation'),
    t('feature_class'),
    t('feature_code'),
    t('latitude'),
    t('longitude'),
    t('modification date'),
    t('population'),
    t('timezone'),
    t('distance'),
    t('continent'),
    t('All'),
    t('Coordinates Feature'),
  );
  $export['geonames_feature'] = $view;

  return $export;
}
