<?php
/**
 * @file
 * geonames_local.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function geonames_local_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'geonames_countryinfo';
  $feeds_importer->config = array(
    'name' => 'Geonames Country Info',
    'description' => 'Geonames Country Info Import',
    'fetcher' => array(
      'plugin_key' => 'FeedsGeonamesCountryInfoDataFetcher',
      'config' => array(
        'source' => 'http://download.geonames.org/export/dump/countryInfo.txt',
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsGeonamesCountryInfoParser',
      'config' => array(
        'delimiter' => 'TAB',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsEntityProcessorData_geonames_countryinfo',
      'config' => array(
        'mappings' => array(
          'guid' => array(
            'source' => 'geonameid',
            'target' => 'guid',
            'unique' => 1,
          ),
          'geonameid' => array(
            'source' => 'geonameid',
            'target' => 'geonameid',
            'unique' => 1,
          ),
          'iso_alpha2' => array(
            'source' => 'ISO',
            'target' => 'iso_alpha2',
            'unique' => 0,
          ),
          'iso_alpha3' => array(
            'source' => 'ISO3',
            'target' => 'iso_alpha3',
            'unique' => 0,
          ),
          'iso_numeric' => array(
            'source' => 'ISO-Numeric',
            'target' => 'iso_numeric',
            'unique' => 0,
          ),
          'fips_code' => array(
            'source' => 'fips',
            'target' => 'fips_code',
            'unique' => 0,
          ),
          'name' => array(
            'source' => 'Country',
            'target' => 'name',
            'unique' => 0,
          ),
          'capital' => array(
            'source' => 'Capital',
            'target' => 'capital',
            'unique' => 0,
          ),
          'areainsqkm' => array(
            'source' => 'Area(in sq km)',
            'target' => 'areainsqkm',
            'unique' => 0,
          ),
          'population' => array(
            'source' => 'Population',
            'target' => 'population',
            'unique' => 0,
          ),
          'continent' => array(
            'source' => 'Continent',
            'target' => 'continent',
            'unique' => 0,
          ),
          'tld' => array(
            'source' => 'tld',
            'target' => 'tld',
            'unique' => 0,
          ),
          'currency' => array(
            'source' => 'CurrencyCode',
            'target' => 'currency',
            'unique' => 0,
          ),
          'currencyname' => array(
            'source' => 'CurrencyName',
            'target' => 'currencyname',
            'unique' => 0,
          ),
          'phone' => array(
            'source' => 'Phone',
            'target' => 'phone',
            'unique' => 0,
          ),
          'postal_code_format' => array(
            'source' => 'Postal Code Format',
            'target' => 'postal_code_format',
            'unique' => 0,
          ),
          'postal_code_regex' => array(
            'source' => 'Postal Code Regex',
            'target' => 'postal_code_regex',
            'unique' => 0,
          ),
          'languages' => array(
            'source' => 'Languages',
            'target' => 'languages',
            'unique' => 0,
          ),
          'neighbours' => array(
            'source' => 'neighbours',
            'target' => 'neighbours',
            'unique' => 0,
          ),
          'equivalentfipscode' => array(
            'source' => 'EquivalentFipsCode',
            'target' => 'equivalentfipscode',
            'unique' => 0,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'data_geonames_countryinfo',
        'values' => array(),
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '2419200',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['geonames_countryinfo'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'geonames_feature';
  $feeds_importer->config = array(
    'name' => 'Geonames Feature',
    'description' => 'Geonames Feature Import',
    'fetcher' => array(
      'plugin_key' => 'FeedsGeonamesFeatureDataFetcher',
      'config' => array(
        'source' => 'http://download.geonames.org/export/dump/allCountries.zip',
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'force_download' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsGeonamesFeatureParser',
      'config' => array(
        'feature_codes' => array(
          'A.ADM1' => 'A.ADM1',
          'A.ADM1H' => 'A.ADM1H',
          'A.ADM2' => 'A.ADM2',
          'A.ADM2H' => 'A.ADM2H',
          'A.ADM3' => 'A.ADM3',
          'A.ADM3H' => 'A.ADM3H',
          'A.ADM4' => 'A.ADM4',
          'A.ADM4H' => 'A.ADM4H',
          'A.ADM5' => 'A.ADM5',
          'A.ADM5H' => 'A.ADM5H',
          'A.ADMD' => 'A.ADMD',
          'A.ADMDH' => 'A.ADMDH',
          'A.LTER' => 'A.LTER',
          'A.PCL' => 'A.PCL',
          'A.PCLD' => 'A.PCLD',
          'A.PCLF' => 'A.PCLF',
          'A.PCLH' => 'A.PCLH',
          'A.PCLI' => 'A.PCLI',
          'A.PCLIX' => 'A.PCLIX',
          'A.PCLS' => 'A.PCLS',
          'A.PRSH' => 'A.PRSH',
          'A.TERR' => 'A.TERR',
          'A.ZN' => 'A.ZN',
          'A.ZNB' => 'A.ZNB',
          'P.PPL' => 'P.PPL',
          'P.PPLA' => 'P.PPLA',
          'P.PPLA2' => 'P.PPLA2',
          'P.PPLA3' => 'P.PPLA3',
          'P.PPLA4' => 'P.PPLA4',
          'P.PPLC' => 'P.PPLC',
          'P.PPLCH' => 'P.PPLCH',
          'P.PPLF' => 'P.PPLF',
          'P.PPLG' => 'P.PPLG',
          'P.PPLH' => 'P.PPLH',
          'P.PPLL' => 'P.PPLL',
          'P.PPLQ' => 'P.PPLQ',
          'P.PPLR' => 'P.PPLR',
          'P.PPLS' => 'P.PPLS',
          'P.PPLW' => 'P.PPLW',
          'P.PPLX' => 'P.PPLX',
          'P.STLMT' => 'P.STLMT',
        ),
        'no_headers' => 1,
        'delimiter' => 'TAB',
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsEntityProcessorData_geonames_feature',
      'config' => array(
        'mappings' => array(
          0 => array(
            'source' => '0',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => '0',
            'target' => 'geonameid',
            'unique' => 1,
          ),
          2 => array(
            'source' => '1',
            'target' => 'name',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => '2',
            'target' => 'asciiname',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => '4',
            'target' => 'latitude',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => '5',
            'target' => 'longitude',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => '6',
            'target' => 'feature_class',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => '7',
            'target' => 'feature_code',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => '8',
            'target' => 'country_code',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => '9',
            'target' => 'cc2',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => '10',
            'target' => 'admin1_code',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => '11',
            'target' => 'admin2_code',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => '12',
            'target' => 'admin3_code',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => '13',
            'target' => 'admin4_code',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => '14',
            'target' => 'population',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => '15',
            'target' => 'elevation',
            'unique' => FALSE,
          ),
          16 => array(
            'source' => '16',
            'target' => 'dem',
            'unique' => FALSE,
          ),
          17 => array(
            'source' => '17',
            'target' => 'timezone',
            'unique' => FALSE,
          ),
          18 => array(
            'source' => '18',
            'target' => 'modification_date',
            'unique' => FALSE,
          ),
          19 => array(
            'source' => '4',
            'target' => 'lat_lon_pair',
            'unique' => FALSE,
          ),
          20 => array(
            'source' => 'blank source 1',
            'target' => 'country_geonameid',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'data_geonames_feature',
        'values' => array(
          'geonameid' => '',
          'name' => '',
          'asciiname' => '',
          'alternatenames' => '',
          'latitude' => '',
          'longitude' => '',
          'feature_class' => '',
          'feature_code' => '',
          'country_code' => '',
          'cc2' => '',
          'admin1_code' => '',
          'admin2_code' => '',
          'admin3_code' => '',
          'admin4_code' => '',
          'population' => '',
          'elevation' => '',
          'dem' => '',
          'timezone' => '',
          'changed' => '1970-01-01',
        ),
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '2419200',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['geonames_feature'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'geonames_postalcode';
  $feeds_importer->config = array(
    'name' => 'Geonames Postalcode',
    'description' => 'Geonames Postalcode Import',
    'fetcher' => array(
      'plugin_key' => 'FeedsGeonamesPostalCodeDataFetcher',
      'config' => array(
        'source' => 'http://download.geonames.org/export/zip/allCountries.zip',
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsGeonamesPostalCodeParser',
      'config' => array(
        'no_headers' => 1,
        'delimiter' => 'TAB',
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsEntityProcessorData_geonames_postalcode',
      'config' => array(
        'mappings' => array(
          0 => array(
            'source' => '0',
            'target' => 'country_code',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => '1',
            'target' => 'postal_code',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => '2',
            'target' => 'place_name',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => '3',
            'target' => 'admin_name1',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => '4',
            'target' => 'admin_code1',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => '5',
            'target' => 'admin_name2',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => '6',
            'target' => 'admin_code2',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => '7',
            'target' => 'admin_name3',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => '8',
            'target' => 'admin_code3',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => '9',
            'target' => 'latitude',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => '10',
            'target' => 'longitude',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => '11',
            'target' => 'accuracy',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'blank source 1',
            'target' => 'country_geonameid',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'blank source 2',
            'target' => 'lat_lon_pair',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'blank source 3',
            'target' => 'guid',
            'unique' => 1,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'data_geonames_postalcode',
        'values' => array(),
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '2419200',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['geonames_postalcode'] = $feeds_importer;

  return $export;
}
