<?php

/**
 * @file
 * Home of the FeedsGeonamesDataFetcher and related classes.
 */

/**
 * Fetches data via HTTP.
 */
class FeedsGeonamesPostalCodeDataFetcher extends FeedsGeonamesBaseDataFetcher {

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);
    // Just allow postalcodes sources as option.
    $options = geonames_local_sources();
    $form['source']['#options'] = $options['postalcodes'];
    return $form;
  }

  /**
   * Override parent::configForm().
   */
  public function sourceForm($source_config) {
    $form = parent::sourceForm($source_config);
    // Just allow postalcodes sources as option.
    $options = geonames_local_sources();
    $form['source']['#options'] = $options['postalcodes'];
    return $form;
  }
}
