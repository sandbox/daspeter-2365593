<?php

/**
 * @file
 * Contains the FeedsGeonamesBase class.
 */

/**
 * Parses a given file as a CSV file.
 */
abstract class FeedsGeonamesBaseParser extends FeedsCSVParser {

  /**
   * Fetch the iterator to use for this implementation.
   *
   * Enables us to define filter iterators.
   *
   * @return Iterator
   *   The iterator to use to iterate over the lines in the CSV.
   */
  abstract protected function getIteratorIterator(Iterator $iterator);

  /**
   * Override parent::getMappingSources().
   */
  public function getMappingSources() {
    self::loadMappers();
    $sources = array();
    $content_type = feeds_importer($this->id)->config['content_type'];
    drupal_alter('feeds_parser_sources', $sources, $content_type);
    return $sources;
  }

  /**
   * Parse all of the items from the CSV.
   */
  protected function parseItems(ParserCSV $parser, ParserCSVIterator $iterator, $start = 0, $limit = 0) {
    $parser->setLineLimit($limit * 4);
    $parser->setStartByte($start);
    $iterator_iterator = $this->getIteratorIterator($iterator);
    $rows = $parser->parse($iterator_iterator);
    return $rows;
  }

  /**
   * Source form.
   *
   * Show mapping configuration as a guidance for import form users.
   */
  public function sourceForm($source_config) {
    $source_config = $this->sourceDefaults() + $source_config;
    $form = parent::sourceForm($source_config);
    $form['delimiter']['#type'] = 'value';
    $form['no_headers']['#type'] = 'value';
    return $form;
  }

  /**
   * Define default configuration.
   */
  public function configDefaults() {
    return array(
      'delimiter' => 'TAB',
    ) + parent::configDefaults();
  }

  /**
   * Build configuration form.
   */
  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);
    $form['delimiter']['#type'] = 'value';
    $form['no_headers']['#type'] = 'value';
    return $form;
  }
}
