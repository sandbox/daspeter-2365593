<?php

/**
 * @file
 * Contains the FeedsGeonamesPostalCodeParser class.
 */

/**
 * Parses a given file as a CSV file.
 */
class FeedsGeonamesPostalCodeParser extends FeedsGeonamesBaseParser {

  /**
   * Returns the iterator to use.
   */
  protected function getIteratorIterator(Iterator $iterator) {
    return $iterator;
  }

  /**
   * Define defaults.
   */
  public function sourceDefaults() {
    return array(
      'no_headers' => 1,
    ) + parent::sourceDefaults();
  }

  /**
   * Define default configuration.
   */
  public function configDefaults() {
    return array(
      'no_headers' => 1,
    ) + parent::configDefaults();
  }
}
