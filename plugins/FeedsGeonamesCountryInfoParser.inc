<?php

/**
 * @file
 * Contains the FeedsGeonamesCountryInfoParser class.
 */

/**
 * Parses a given file as a CSV file.
 */
class FeedsGeonamesCountryInfoParser extends FeedsGeonamesBaseParser {

  /**
   * Return the iterator to use.
   */
  protected function getIteratorIterator(Iterator $iterator) {
    return $iterator;
  }
}
