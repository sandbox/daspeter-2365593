<?php

/**
 * @file
 * Contains the FeedsCSVParser class.
 */

/**
 * Parses a given file as a CSV file.
 */
class FeedsGeonamesFeatureParser extends FeedsGeonamesBaseParser {

  /**
   * Returns the iterator to use.
   */
  protected function getIteratorIterator(Iterator $iterator) {
    return new ParserGeonamesFeatureIterator($iterator, $this->config['feature_codes']);
  }

  /**
   * Define defaults.
   */
  public function sourceDefaults() {
    return array(
      'feature_codes' => $this->config['feature_codes'],
      'no_headers' => 1,
    ) + parent::sourceDefaults();
  }

  /**
   * Source form.
   *
   * Show mapping configuration as a guidance for import form users.
   */
  public function sourceForm($source_config) {
    $source_config += $this->getConfig();
    $form = parent::sourceForm($source_config);

    $feature_codes_options = geonames_local_feature_code_options();
    $default_codes = array_keys($feature_codes_options['P']);
    $form['feature_codes'] = array(
      '#type' => 'select',
      '#title' => t('Feature to import'),
      '#description' => t('Define which features to import. Details about features codes can be found <a href="!url" target="_blank">here</a>. Leave empty for no limitation.', array('!url' => 'http://www.geonames.org/export/codes.html')),
      '#options'  => $feature_codes_options,
      '#default_value' => isset($source_config['feature_codes']) ? $source_config['feature_codes'] : $default_codes,
      '#multiple' => TRUE,
      '#chosen' => FALSE,
      '#size' => 30,
      '#exclude_cv' => TRUE,
    );
    return $form;
  }

  /**
   * Define default configuration.
   */
  public function configDefaults() {
    $feature_classes_options = geonames_local_feature_code_options();
    $feature_codes_options = array();
    if (isset($feature_classes_options['P'])) {
      $feature_codes_options = array_keys($feature_classes_options['P']);
    }
    return array(
      'feature_codes' => $feature_codes_options,
      'no_headers' => 1,
    ) + parent::configDefaults();
  }

  /**
   * Build configuration form.
   */
  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);

    $feature_codes_options = geonames_local_feature_code_options();
    $default_codes = array_keys($feature_codes_options['P']);
    $form['feature_codes'] = array(
      '#type' => 'select',
      '#title' => t('Features to import'),
      '#description' => t('Define which features to import. Details about features codes can be found <a href="!url" target="_blank">here</a>. Leave empty for no limitation.', array('!url' => 'http://www.geonames.org/export/codes.html')),
      '#options'  => $feature_codes_options,
      '#default_value' => isset($this->config['feature_codes']) ? $this->config['feature_codes'] : $default_codes,
      '#multiple' => TRUE,
      '#chosen' => FALSE,
      '#size' => 30,
      '#exclude_cv' => TRUE,
    );
    return $form;
  }
}

/**
 * Class ParserGeonamesFeatureIterator
 */
class ParserGeonamesFeatureIterator extends FilterIterator  {

  /**
   * Defines the feature code to filter.
   *
   * Filter is disabled if no feature codes are defined.
   *
   * @var array
   */
  protected $featureCodes = array();

  /**
   * Store the feature codes to filter for.
   */
  public function __construct($iterator, $feature_codes = array()) {
    parent::__construct($iterator);
    $this->featureCodes = $feature_codes;
  }

  /**
   * Check if this line can be imported.
   */
  public function accept() {
    // If feature codes are given, but the feature code isn't found skip this
    // item.
    $line = $this->getInnerIterator()->current();
    $line_arr = explode("\t", $line);
    if (!empty($this->featureCodes) && !in_array($line_arr[6] . '.' . $line_arr[7], $this->featureCodes)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Special version of rewind that accepts a position parameter.
   */
  public function rewind($pos = 0) {
    parent::rewind();
    parent::getInnerIterator()->rewind($pos);
  }
}
