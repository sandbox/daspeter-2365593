<?php

/**
 * @file
 * Home of the FeedsGeonamesDataFetcher and related classes.
 */

/**
 * Fetches data via HTTP.
 */
class FeedsGeonamesBaseDataFetcher extends FeedsHTTPFetcher {

  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $config = $source->getConfigFor($this);
    $fetcher_result = new FeedsGeonamesBaseDataFetcherResult($config['source'], $config['force_download']);
    // When request_timeout is empty, the global value is used.
    $fetcher_result->setTimeout($this->config['request_timeout']);
    $fetcher_result->setAcceptInvalidCert($this->config['accept_invalid_cert']);
    return $fetcher_result;
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'source' => '',
      'force_download' => FALSE,
    ) + parent::configDefaults();
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);
    $form['auto_detect_feeds']['#type'] = 'value';
    $form['advanced']['auto_scheme']['#type'] = 'value';
    $form['use_pubsubhubbub']['#type'] = 'value';
    $form['advanced']['designated_hub']['#type'] = 'value';

    $form['source'] = array(
      '#type' => 'select',
      '#title' => t('Source'),
      '#default_value' => isset($this->config['source']) ? $this->config['source'] : '',
      '#required' => TRUE,
      '#options' => geonames_local_sources(),
    );
    return $form;
  }

  /**
   * Override parent::configDefaults().
   */
  public function sourceDefaults() {
    return $this->config + parent::sourceDefaults() + array('force_download');
  }

  /**
   * Expose source form.
   */
  public function sourceForm($source_config) {
    $source_config += $this->config;
    $form = parent::sourceForm($source_config);
    $form['source'] = array(
      '#type' => 'select',
      '#title' => t('Source'),
      '#default_value' => isset($source_config['source']) ? $source_config['source'] : $this->confg['source'],
      '#required' => TRUE,
      '#options' => geonames_local_sources(),
    );
    $form['force_download'] = array(
      '#type' => 'checkbox',
      '#title' => t('Forces the download of the file. Otherwise the same file is used for a week.'),
      '#default_value' => isset($source_config['force_download']) ? $source_config['force_download'] : 0,
    );
    return $form;
  }
}

/**
 * Result of FeedsHTTPFetcher::fetch().
 */
class FeedsGeonamesBaseDataFetcherResult extends FeedsHTTPFetcherResult {

  protected $force = FALSE;

  /**
   * Constructor.
   */
  public function __construct($url = NULL, $force = FALSE) {
    parent::__construct($url);
    $this->force = $force;
  }

  /**
   * Overwrite the default handling of creating files.
   *
   * We want teh same file name.
   */
  public function getFilePath() {
    if (!isset($this->file_path)) {
      $destination = 'public://feeds';
      if (!file_prepare_directory($destination, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
        throw new Exception(t('Feeds directory either cannot be created or is not writable.'));
      }
      $this->file_path = $file_path = $destination . '/' . get_class($this) . '_' . preg_replace('/^.+?([^\/]+)$/', '$1', $this->url);
      if (!file_exists($file_path) || filemtime($file_path) < strtotime('today last week') || $this->force) {
        $this->file_path = FALSE;
        if (!$this->file_path = file_unmanaged_save_data($this->getRaw(), $file_path, FILE_EXISTS_REPLACE)) {
          throw new Exception(t('Cannot write content to %dest', array('%dest' => $destination)));
        }
      }
    }
    return $this->sanitizeFile($this->file_path);
  }

  /**
   * Overrides FeedsHTTPFetcherResult::sanitizeFile().
   *
   * Ensure archives are extracted.
   */
  public function sanitizeFile($filepath) {
    $pathinfo = pathinfo($this->url);

    // If it is an archive - extract it.
    if ($pathinfo['extension'] == 'zip') {
      $filepath = drupal_realpath($filepath);
      // Extract file.
      if ($archiver = archiver_get_archiver($filepath)) {
        // Likely a huge file - so manipulate the timeout.
        drupal_set_time_limit(0);
        $filename = $pathinfo['filename'] . '.txt';
        $target_file = dirname($filepath) . '/' . crc32($this->url) . '_' . $filename;
        // Extract it only if necessary.
        // The filemtime() helps to avoid the file is extracted over and over
        // again during the import.
        if (!file_exists($target_file) || filemtime($target_file) < strtotime('today last week')) {
          $extracted_file = dirname($filepath) . '/' . $filename;
          if (file_exists($target_file)) {
            drupal_unlink($target_file);
          }
          if (file_exists($extracted_file)) {
            drupal_unlink($extracted_file);
          }
          $archiver->extract(dirname($filepath), array($filename));
          clearstatcache();
          if (!file_exists($extracted_file)) {
            throw new Exception(t('Extract content from %dest failed.', array('%dest' => $filename)));
          }
          file_unmanaged_move($extracted_file, $target_file, FILE_EXISTS_REPLACE);
        }
        $filepath = $target_file;
      }
      else {
        throw new Exception(t('Cannot extract content from %dest. Please ensure the php zip extension is enabled.', array('%dest' => $pathinfo['basename'])));
      }
    }
    return parent::sanitizeFile($filepath);
  }
}
