<?php

/**
 * @file
 * Home of the FeedsGeonamesDataFetcher and related classes.
 */

/**
 * Fetches data via HTTP.
 */
class FeedsGeonamesFeatureDataFetcher extends FeedsGeonamesBaseDataFetcher {

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);
    // Just allow feature sources as option.
    $options = geonames_local_sources();
    $form['source']['#options'] = $options['features'];
    return $form;
  }

  /**
   * Override parent::configForm().
   */
  public function sourceForm($source_config) {
    $form = parent::sourceForm($source_config);
    // Just allow feature sources as option.
    $options = geonames_local_sources();
    $form['source']['#options'] = $options['features'];
    return $form;
  }
}
