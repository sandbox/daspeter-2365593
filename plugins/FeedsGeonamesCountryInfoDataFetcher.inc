<?php

/**
 * @file
 * Home of the FeedsGeonamesCountryInfoDataFetcher and related classes.
 */

/**
 * Fetches data via HTTP.
 */
class FeedsGeonamesCountryInfoDataFetcher extends FeedsGeonamesBaseDataFetcher {

  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $config = $source->getConfigFor($this);
    $fetcher_result = new FeedsGeonamesCountryInfoDataFetcherResult($config['source'], $config['force_download']);
    // When request_timeout is empty, the global value is used.
    $fetcher_result->setTimeout($this->config['request_timeout']);
    $fetcher_result->setAcceptInvalidCert($this->config['accept_invalid_cert']);
    return $fetcher_result;
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);
    // Just allow metadata sources as option.
    $options = geonames_local_sources();
    $form['source']['#options'] = $options['metadata'];
    return $form;
  }

  /**
   * Override parent::configForm().
   */
  public function sourceForm($source_config) {
    $form = parent::sourceForm($source_config);
    // Just allow metadata sources as option.
    $options = geonames_local_sources();
    $form['source']['#options'] = $options['metadata'];
    return $form;
  }
}


class FeedsGeonamesCountryInfoDataFetcherResult extends FeedsGeonamesBaseDataFetcherResult {
  /**
   * Remove documentation header of the country info file.
   */
  public function getRaw() {
    $init = !isset($this->raw);
    $output = parent::getRaw();
    if ($init) {
      // Country information - strip the documentation part.
      $this->raw = mb_substr($this->raw, mb_stripos($this->raw, "#ISO\tISO3\tISO-Numeric") + 1);
      $output = $this->sanitizeRaw($this->raw);
    }
    return $output;
  }
}
