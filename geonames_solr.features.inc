<?php
/**
 * @file
 * geonames_solr.features.inc
 */

/**
 * Implements hook_views_api().
 */
function geonames_solr_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_search_api_index().
 */
function geonames_solr_default_search_api_index() {
  $items = array();
  $items['geonames_feature'] = entity_import('search_api_index', '{
    "name" : "Geonames Feature",
    "machine_name" : "geonames_feature",
    "description" : "Geonames Features Index",
    "server" : "localhost",
    "item_type" : "data_geonames_feature",
    "options" : {
      "index_directly" : 0,
      "cron_limit" : "500",
      "fields" : {
        "country_code" : { "type" : "string" },
        "country_geonameid:continent" : { "type" : "string" },
        "elevation" : { "type" : "decimal" },
        "feature_class" : { "type" : "string" },
        "feature_code" : { "type" : "string" },
        "geonameid" : { "type" : "string" },
        "lat_lon_pair" : { "type" : "string", "real_type" : "location" },
        "latitude" : { "type" : "decimal" },
        "longitude" : { "type" : "decimal" },
        "modification_date" : { "type" : "date" },
        "name" : { "type" : "string" },
        "postalcode:postal_code" : { "type" : "string" },
        "search_api_language" : { "type" : "string" },
        "timezone" : { "type" : "text" }
      },
      "data_alter_callbacks" : {
        "search_api_ranges_alter" : { "status" : 0, "weight" : "-50", "settings" : [] },
        "search_api_alter_language_control" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "lang_field" : "", "languages" : [] }
        },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "Tokens" } },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_denormalized_entity_grouping" : {
          "status" : 0,
          "weight" : "0",
          "settings" : {
            "fields" : [],
            "group_sort" : "",
            "group_sort_direction" : "asc",
            "truncate" : 1,
            "group_limit" : "1"
          }
        },
        "search_api_case_ignore" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "fields" : { "name" : true } }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "name" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_transliteration" : {
          "status" : 0,
          "weight" : "15",
          "settings" : { "fields" : { "name" : true } }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "name" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "name" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "exclude_fields" : [],
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  return $items;
}
